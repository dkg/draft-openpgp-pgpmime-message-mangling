---
title: Common PGP/MIME Message Mangling
docname: draft-dkg-openpgp-pgpmime-message-mangling-00
date: 2019-05-22
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
informative:
 RFC1939:
 RFC4880:
 RFC5321:
 RFC7601:
normative:
 RFC1847:
 RFC2119:
 RFC3156:
 RFC8174:
--- abstract

An e-mail message with OpenPGP encryption and/or signature has
standard form.  However, some mail transport agents damage those forms
in transit.  A user-friendly mail user agent that encounters such a
mangled message may wish to try to process it anyway, despite the
message's non-standard structure.

This document aims to be a sort of bestiary of common message mangling
patterns, along with guidance for implementers for how to cope with
messages structured in these common ways.

--- middle

Introduction
============

Requirements Language
---------------------

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
BCP 14 {{RFC2119}} {{RFC8174}} when, and only when, they appear in all
capitals, as shown here.

Terminology
-----------

 * `Mail User Agent` (or `MUA`) refers to a program designed to send
   and receive e-mail.  Common examples include Thunderbird, Outlook,
   and Mail.app.  Webmail implementations, like Roundcube or the Gmail
   interface can also be considered a MUA.
   
 * `Mail Transport Agent` (or `MTA`) refers to mail-handling software
   within the network.  Common examples include Postfix and Exchange.
 
 * `PGP/MIME` is the message structure for OpenPGP
   protections described in {{RFC3156}}.
 
 * `encrypted message` is used to mean any message where the outermost
   PGP/MIME cryptographic feature of the message itself is encryption.
   Some encrypted messages might be signed, but the inner signature
   cannot typically be mangled by an `MTA` that can't decrypt.
 
 * `clearsigned message` is used here to refer specifically to
   messages that are not encrypted, but are signed with a PGP/MIME
   signature.
   
 * `signed message` is used to refer to a message that has a valid
   OpenPGP cryptographic signature applied by the message sender,
   whether the message is encrypted or not.

Problem Statement
=================

Some MTAs modify message headers, bodies, and structure in transit.
Some of those modifications ("manglings") can transform a message in
such a way that the end-to-end cryptographic properties of the message
are lost.

Clearsigned messages can become unverifiable, and encrypted messages
can become impossible to decrypt.  MUAs that receive these mangled
messages are unable to fulfil their users goals because of damage done
to the message by the mangling MTA.

In some cases, those manglings can take a regular enough form that a
clever MUA can "repair" them, restoring the cryptographic properties
of the message for the user.

This document aims to collect common manglings, and document how an
MUA can detect them and (if possible) repair them safely for the user.

Cryptographic Properties
========================

TODO: describe message-wide cryptographic envelope vs. cryptographic
payload and their relevance to this draft.

TODO: we're assuming a single layer of cryptographic protection.
These steps get more complicated if there are multiple nested layers
(encrypted and then signed?  signed and then encrypted?
triple-wrapped?).  Should this document address that situation?

Encrypted Messages
==================

This section aims to collect examples of repairable mangling known to
be performed by some currently-active MTA on encrypted messages.  Each
section offers a brief description of the mangling, a way to detect
it, and a proposed method of repair.

"Mixed Up" Encryption {#mixed-up}
---------------------

One common mangling takes an incoming multipart/encrypted e-mail and
transforms it into a multipart/mixed e-mail, shuffling body parts at
the same time.

An encrypted message typically has this clean structure C (see example
in {{encrypted-message-sample}}):

    C   multipart/encrypted
    C.1  application/pgp-encrypted
    C.2  application/octet-stream

But after processing, it has mangled structure M (see example in
{{mixup-message-sample}}):

    M   multipart/mixed
    M.1  text/plain (0 bytes)
    M.2  application/pgp-encrypted
    M.3  application/octet-stream

Some versions of Microsoft Exchange are known to do this.

### Detection of "Mixed Up" Encryption

Check that all of the following conditions hold:

 * The message itself (M) is Content-Type: multipart/mixed.
 
 * The message has exactly three MIME subparts, all direct children of
   the message itself.
 
 * The first part (M.1) is Content-Type: text/plain, and has 0 bytes.
 
 * The second part (M.2) is Content-Type: application/pgp-encrypted,
   and contains (after removing any Content-Transfer-Encoding) exactly
   the string "Version: 1"
 
 * The third part (M.3) is Content-Type: application/octet-stream.
 
 * After removing any Content-Transfer-Encoding, the decoded third
   part (M.3) is an ASCII-armored OpenPGP block of type "PGP MESSAGE"
   (see see section 6.2 of {{RFC4880}})
   
Please see {{detecting-mixed-up-python}} for an example implementation.

### Repair of "Mixed Up" Encryption

If the detection is successful, repair can be attempted with the
following steps:

 * Convert the message's Content-Type: value to multipart/encrypted,
   while leaving any other parameters untouched.
 
 * Add (or reset) the parameter protocol=application/pgp-encrypted to
   the Content-Type: header.
 
 * Drop the first sub-part (M.1).

Please see {{repairing-mixed-up-python}} for an example implementation.

iPGMail/iOS Encryption Mangling
-------------------------------

TODO: document iPGMail breakage when delivered through iOS default
mailer (iCloud?), as documented in:
https://sourceforge.net/p/enigmail/forum/support/thread/afc9c246/#5de7


Clearsigned Messages
====================

This section aims to collect examples of repairable mangling known to
be performed by some currently-active MTA on clearsigned messages.  Each
section offers a brief description of the mangling, a way to detect
it, and a proposed method of repair.

Extra MIME footer
-----------------

TODO: describe the mailman case

Content-Re-Encoding
-------------------

TODO: despite the content of multipart/signed parts being ostensibly
invariant (see page 4 of {{RFC1847}}), some MTAs mangle them.
Describe some reversible manglings, like gratuitous application of
Content-Transfer-Encoding.

Doubled Dots {#doubled-dots}
------------

The signature may be broken by SMTP and POP3 transactions if any line
in the data stream starts with a period (".", ASCII 0x2e) character.

In this case, Section 4.5.2 of {{RFC5321}} and Section 3 of
{{RFC1939}} require that the period is doubled for the transmission,
which the receiving party shall remove.  If the receiving party fails
to do so, obviously the signature can no longer be validated.

See the example in {{doubled-dots-sample}}.

### Detecting Doubled Dots

If a multipart/signed message fails to validate, and any of the lines
in the raw message source start with at least two dots (".", ASCII
0x2e), then the receiving MUA may suspect that this form of mangling
has happened.

It can confirm that this is the case only by attempting signature
verification over a modified message source with one extra leading dot
removed from each line where two or more are present.  If the
signature is valid over the modified variant, then this mangling has
happened.  If the signature cannot be validated, it does not mean that
the mangling did not happen, though, as there could be additional
mangling.

### Repairing Doubled Dots

If the signature validates over the modified variant with all doubled
leading dots reduced by a single leading dot, then a reasonable MUA
MAY consider the correct form of the message to be the modified form.

Multiple Manglings
==================

The mangling types described in this draft are minimal descriptions.
It's possible for a single message to be mangled in multiple ways, or
for a given mangling to happen more than once.

TODO: how should reasonable MUAs attempt multiple repairs, or recursve
repairs?  for example, if there is a line that starts with a row of 10
dots (".........."), should the MUA attempt 10 signature verification
attempts, removing one leading dot each time?

Performance Considerations
==========================

Trying multiple repair techniques can be expensive.  A
resource-constrained MUA may want to limit itself to detection of
possible mangling, rather than trying every possible repair it knows
of.

Some when attempting to repair a clearsigned message, an MUA can avoid
incurring most of the costs of asymmetric cryptographic signature
verification by hashing each proposed variant and comparing its
leftmost 16 bits with the signature's "two-octet field holding the
left 16 bits of the signed hash value" as described in section 5.2.3
of {{RFC4880}}.


Security Considerations
=======================

There are several ways that attempting repair on a mangled message can
go dangerously wrong.

Only Repair for Cryptographic Improvement
-----------------------------------------

If a message repair produces a seemingly-better-structured message,
but the repaired message still cannot be decrypted or verified, then
the MUA should abandon the repair and instead render the message in
its original form.  Additional message modification that does not
provide a cryptographic advantage over the received form of the
message offers no benefit to the user.

TODO: is a decryptable, unsigned message "better" than a verifiably
signed message?  What is "an improvement" isn't necessarily clear here.

Avoiding Another E-Fail
-----------------------

Promiscuous message repair may inject new vulnerabilities, or modify a
message beyond recognition.  An MUA attempting message repair should
use caution to avoid re-combining potentially malicious material with
material that has better cryptographic guarantees.

In particular, a responsible MUA should take care not to claim
cryptographic protections over material that does not have them.

Do Not Attempt Repair Inside End-to-End Encryption
--------------------------------------------------

If a message or part of a message arrived end-to-end encrypted, none
of the recommendations in this draft should be read as encouraging
their application to any cleartext material within that end-to-end
encryption.  These techniques are specifically for recovering from
damage done by the MTA, which by design does not have access to the
cleartext of an end-to-end encrypted message.

Interactions with DKIM Verification
-----------------------------------

A mangling MTA may also add DKIM headers after mangling.  A subsequent
MTA that handles the message may verify the message by checking DKIM
headers, storing the results of that check in an
Authentication-Results header (see {{RFC7601}}). If the MUA performs
message repair, the repaired message might not pass DKIM check.  An
MUA that relies on Authentication-Results headers for any purpose on
such a repaired message should consider re-validating the DKIM header
itself directly after message repair (though this introduces new
problems for stored/old messages, as older DKIM signing keys may no
longer be published in the DNS).

Note that a repaired message may have cryptographic properties that
obviates the need for DKIM verification; an MUA that requires messages
to have a valid Authentication-Results: header might waive that
requirement for a signed message (whether any repair routine was
invoked or not).

"Helpful" MTA mangling
----------------------

An MTA may intend to help the user by modifying the message.  For
example, in a clearsigned message, the MTA may modify a
potentially-malicious URL in the text, or it might change an
attachment name or MIME-type in an attachment.  If an MUA attempts
message repair in a way that reverses these modifications, it may
negate whatever security advantages the MTA hoped to offer.

It is not clear that this is a risk for repair of an encrypted
message, though, since the MTA by design cannot perform such a
modification on the cleartext of an encrypted message.

Privacy Considerations
======================

TODO: privacy considerations?

User Considerations
===================

Cryptographic properties of messages are only useful if the user
understands them.

Indications of Mangling Can Be Confusing
----------------------------------------

* TODO: about how indicators of mangled messages can cause
  user-experience confusion (e.g. parts of https://efail.de/ and
  https://github.com/RUB-NDS/Johnny-You-Are-Fired )

Repair Indicators
-----------------

* TODO: how should a repairing MUA indicate that a repair has happened
  (or is possible) to the user?

Undoing Repair
--------------

* TODO: Should a repairing MUA allow the user to reverse the repair
  and see the broken message?  If so, what else should happen if the
  user takes this action (e.g., how is a subsequent "Reply" or
  "Forward" action affected?)

Attempting Decryption During Repair
-----------------------------------

Attempting decryption may require access to secret key material, which
itself may cause interaction with the user.  If multiple repairs are
attempted, each attempt to decrypt may require multiple decryptions.
In some scenarios, multiple use of the secret key may be annoying to
the user.  It might be preferable to try to decrypt the apparent
underlying block of text exactly once, regardless of message structure
or repair, and if successful, either stash the session key, or store
the cleartext in memory, while manipulating exterior structure.

IANA Considerations
===================

This document requires no actions from IANA.

Document Considerations
=======================

\[ RFC Editor: please remove this section before publication ]

This document is currently edited as markdown.  Minor editorial
changes can be suggested via merge requests at
https://gitlab.com/dkg/draft-openpgp-pgpmime-message-mangling or by
e-mail to the author.  Please direct all significant commentary to the
public IETF OpenPGP mailing list: openpgp@ietf.org


Example Messages
================

Example Input Encrypted Message {#encrypted-message-sample}
-------------------------------


    From: Michael Elkins <elkins@aero.org>
    To: Michael Elkins <elkins@aero.org>
    Mime-Version: 1.0
    Content-Type: multipart/encrypted; boundary=foo;
       protocol="application/pgp-encrypted"
   
    --foo
    Content-Type: application/pgp-encrypted
    
    Version: 1
    
    --foo
    Content-Type: application/octet-stream
    
    -----BEGIN PGP MESSAGE-----
    
    hIwDY32hYGCE8MkBA/wOu7d45aUxF4Q0RKJprD3v5Z9K1YcRJ2fve87lMlDlx4Oj
    eW4GDdBfLbJE7VUpp13N19GL8e/AqbyyjHH4aS0YoTk10QQ9nnRvjY8nZL3MPXSZ
    g9VGQxFeGqzykzmykU6A26MSMexR4ApeeON6xzZWfo+0yOqAq6lb46wsvldZ96YA
    AABH78hyX7YX4uT1tNCWEIIBoqqvCeIMpp7UQ2IzBrXg6GtukS8NxbukLeamqVW3
    1yt21DYOjuLzcMNe/JNsD9vDVCvOOG3OCi8=
    =zzaA
    -----END PGP MESSAGE-----
    
    --foo--


Encrypted Message Mangled by "Mixed Up" {#mixup-message-sample}
---------------------------------------

This is the message found in {{encrypted-message-sample}} with the
"Mixed up" encryption mangling (see {{mixed-up}}) applied to it:

    From: Michael Elkins <elkins@aero.org>
    To: Michael Elkins <elkins@aero.org>
    Mime-Version: 1.0
    Content-Type: multipart/mixed; boundary=foo
    
    --foo
    Content-Type: text/plain; charset="us-ascii"
    
    
    --foo
    Content-Type: application/pgp-encrypted
    
    Version: 1
    
    --foo
    Content-Type: application/octet-stream
    
    -----BEGIN PGP MESSAGE-----
    
    hIwDY32hYGCE8MkBA/wOu7d45aUxF4Q0RKJprD3v5Z9K1YcRJ2fve87lMlDlx4Oj
    eW4GDdBfLbJE7VUpp13N19GL8e/AqbyyjHH4aS0YoTk10QQ9nnRvjY8nZL3MPXSZ
    g9VGQxFeGqzykzmykU6A26MSMexR4ApeeON6xzZWfo+0yOqAq6lb46wsvldZ96YA
    AABH78hyX7YX4uT1tNCWEIIBoqqvCeIMpp7UQ2IzBrXg6GtukS8NxbukLeamqVW3
    1yt21DYOjuLzcMNe/JNsD9vDVCvOOG3OCi8=
    =zzaA
    -----END PGP MESSAGE-----
    
    --foo--


Example Signing Key {#signing-key-sample}
-------------------

This public key can be used to verify signatures in these examples.

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    
    mFIEXOdcBhMIKoZIzj0DAQcCAwSkP30p4xDvi/DjCWA9yvLcWEj3rgVaVaJEvEpJ
    AziiZjmAGvIiAXhL/gov2NoT3M63GDVxBfLQXcHyR6hM8ImEtBlBbGljZSA8YWxp
    Y2VAZXhhbXBsZS5uZXQ+iJAEExMIADgCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgEC
    F4AWIQQJJgmW373JwBkZEAcI+sfU8/zcDAUCXOdkVgAKCRAI+sfU8/zcDKjrAP0X
    XFe9c4pBQoabN/rojvWl/S4blWEr6FvTY/trEOH4ggD9H+FlC09CqEGpdPsYaobN
    v+VGVvyzTLnIQQQnIUfwwVQ=
    =Ulxy
    -----END PGP PUBLIC KEY BLOCK-----


Example Input Clearsigned Message {#clearsigned-message-sample}
---------------------------------

This message is signed by (and can be verified with) the public key
found in {{signing-key-sample}}.

    From: Alice <alice@example.net>
    To: Bob <Bob@example.com>
    Message-Id: <clearsigned-message@example.net>
    Date: Thu, 23 May 2019 23:10:18 -0400
    Mime-Version: 1.0
    Content-Type: multipart/signed; micalg=PGP-SHA512;
     protocol="application/pgp-signature"; boundary="xxx"
    
    --xxx
    Content-Type: text/plain; charset=utf-8
    Content-Transfer-Encoding: quoted-printable
    
    Let's dine Thursday at our =E2=80=9Cfavorite=E2=80=9D restaurant=
    .  Do you want to invite Carol?
    
    --xxx
    Content-Type: application/pgp-signature
    
    -----BEGIN PGP SIGNATURE-----
    
    iHUEARMIAB0WIQQJJgmW373JwBkZEAcI+sfU8/zcDAUCXOdh/AAKCRAI+sfU8/zc
    DGA5AQDyd+BduWvwOvdhB/yLMx3fe81MdT1BTilaM0pL9u07GwD8Df/0b2fxKDME
    VHKnoBQSllhY1ubH570KjDOmjVUsBZ4=
    =jjEp
    -----END PGP SIGNATURE-----
    
    --xxx--


Clearsigned Message Mangled by "Doubled Dots" {#doubled-dots-sample}
---------------------------------------------

This is the message found in {{clearsigned-message-sample}} with the
"Doubled Dots" mangling (described in {{doubled-dots}}) applied to it:

    From: Alice <alice@example.net>
    To: Bob <Bob@example.com>
    Message-Id: <clearsigned-message@example.net>
    Date: Thu, 23 May 2019 23:10:18 -0400
    Mime-Version: 1.0
    Content-Type: multipart/signed; micalg=PGP-SHA512;
     protocol="application/pgp-signature"; boundary="xxx"
    
    --xxx
    Content-Type: text/plain; charset=utf-8
    Content-Transfer-Encoding: quoted-printable
    
    Let's dine Thursday at our =E2=80=9Cfavorite=E2=80=9D restaurant=
    ..  Do you want to invite Carol?
    
    --xxx
    Content-Type: application/pgp-signature
    
    -----BEGIN PGP SIGNATURE-----
    
    iHUEARMIAB0WIQQJJgmW373JwBkZEAcI+sfU8/zcDAUCXOdh/AAKCRAI+sfU8/zc
    DGA5AQDyd+BduWvwOvdhB/yLMx3fe81MdT1BTilaM0pL9u07GwD8Df/0b2fxKDME
    VHKnoBQSllhY1ubH570KjDOmjVUsBZ4=
    =jjEp
    -----END PGP SIGNATURE-----
    
    --xxx--

Example Implemenations
======================

These examples use python3.  As Code Examples, they are licensed under
the Simplified BSD License as noted above.

Example: Detecting "Mixed Up" Encryption {#detecting-mixed-up-python}
----------------------------------------

    import re
    import email.message
    
    def is_pgp_message(data: bytes) -> bool:
      lines = data.strip().replace(b'\r\n', b'\n').split(b'\n')
      if lines.pop(0) != b'-----BEGIN PGP MESSAGE-----':
        return False
      while len(lines) and lines.pop(0) != b'':
        pass # discard up to (and including) first blank line
      radix64=rb'[a-zA-Z0-9+/]'
      allradix64 = re.compile(radix64 + rb'+')
      return len(lines) >= 3 and \
        lines.pop() == b'-----END PGP MESSAGE-----' and \
        re.fullmatch(rb'=' + radix64 + rb'{4}', lines.pop()) and \
        re.fullmatch(radix64 + rb'*={0,2}', lines.pop()) and \
        all(filter(lambda l: allradix64.fullmatch(l), lines))
    
    def is_mixed_up(msg: email.message.Message) -> bool:
      if msg.get_content_type() != 'multipart/mixed':
        return False
      pts = msg.get_payload()
      return len(pts) == 3 and \
        pts[0].get_content_type() == 'text/plain' and \
        pts[0].get_payload(decode=True) == b'' and \
        pts[1].get_content_type() == 'application/pgp-encrypted' and \
        pts[1].get_payload(decode=True).strip() == b'Version: 1' and \
        pts[2].get_content_type() == 'application/octet-stream' and \
        is_pgp_message(pts[2].get_payload(decode=True))

Example: Repairing "Mixed Up" Encryption {#repairing-mixed-up-python}
----------------------------------------

    import copy
    import email.message
    import typing
    
    def un_mix_up(msg: email.message.Message) -> \
        typing.Optional[email.message.Message]:
      if is_mixed_up(msg):
        ret = copy.deepcopy(msg)
        ret.set_type('multipart/encrypted')
        ret.set_param('protocol', 'application/pgp-encrypted')
        ret.set_payload(msg.get_payload()[1:])
        return ret
      return None
